<?php

use Kirby\Toolkit\Query;

Kirby::plugin('vitamin2/retrotree', [
    'sections' => [
        'retrotree' => [
            'props' => [
                'headline' => function ($headline = 'Retro Tree') {
                    return $headline;
                },
                'treeMaxDepth' => function ($treeMaxDepth = 3) {
                    return $treeMaxDepth;
                },
                'showContent' => function ($query = 'site') {
                    return $query;
                },
                'showStatus' => function ($showStatus = true) {
                    return $showStatus;
                },
                'showSlug' => function ($showSlug = true) {
                    return $showSlug;
                },
                'treeEndMessage' => function ($treeEndMessage = 'no more entries') {
                    return $treeEndMessage;
                },
                'showPageIcon' => function ($showPageIcon = false) {
                    return $showPageIcon;
                },
                'treeDefaultIcon' => function ($treeDefaultIcon = 'page') {
                    return $treeDefaultIcon;
                },
                'enableTreeCollapse' => function ($enableTreeCollapse = false) {
                    return $enableTreeCollapse;
                },
                'toggleTextOpen' => function ($toggleTextOpen = 'Alles zusammenbrechen') {
                    return $toggleTextOpen;
                },
                'toggleTextClosed' => function ($toggleTextClosed = 'Alles erweitern') {
                    return $toggleTextClosed;
                },
                'enableTreeDisplayDepth' => function ($enableTreeDisplayDepth = true) {
                    return $enableTreeDisplayDepth;
                }
            ],
            'computed' => [

                'tree' => function () {
                    $kirby = new Kirby();

                    if ($this->showContent === "page") {
                        $query = new Query($this->showContent, ['page' => $this->model()]);
                        $page = $query->result();
                        $data = $kirby->site()->find($page)->childrenAndDrafts();
                    } else {
                        $data = $kirby->site()->childrenAndDrafts();
                    }

                    function generateList($data)
                    {
                        $list = [];
                        foreach ($data as $page) {
                            $list[] = [
                                'url'   => (string)$page->url(),
                                'title' => (string)$page->title(),
                                'translations' => $page->translations()->toArray(),
                                'panel' => $page->panel()->url(),
                                'slug' => $page->slug(),
                                'status' => $page->status(),
                                'icon' => $page->panel()->image(),
                                'child' => generateList($page->childrenAndDrafts()),
                                'depth' => $page->depth(),
                            ];
                        }
                        return $list;
                    }
                    $json = generateList($data);
                    $jsonOutput[] = [
                        'data' => $json,
                    ];
                    return $jsonOutput;
                },
            ]
        ]
    ]
]);
